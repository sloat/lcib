import asyncio
import uvloop

uvloop.install()


from .client import IRCClient
from .daemon import CIBDaemon


async def main():
    msgq = asyncio.Queue()

    client = IRCClient(msgq)
    daemon = CIBDaemon(msgq)

    await asyncio.gather(client.run(), daemon.run())

# if __name__ == "__main__":
asyncio.run(main())

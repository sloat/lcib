import asyncio
import time

class IRCClient:

    def __init__(self, queue, **kwargs):
        self.queue = queue
        self.running = False

        self.writer = None
        self.reader = None

        self.config = kwargs


    async def connect(self):
        self.writer, self.reader = await asyncio.open_connection(
                self.config['host'],
                self.config['port']
                )


    async def loop(self):
        while self.running:
            try:
                cmd = await self.queue.get_nowait()
            except asyncio.QueueEmpty:
                pass

            if cmd is not None:

                print('got: ', cmd)

                if cmd == 'stop':
                    self.running = False


    async def run(self):
        print('starting client')
        self.running = True
        await self.connect()
        await self.loop()
        print('stopping client')

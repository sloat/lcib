import uvloop
import asyncio

import argparse
import runpy

uvloop.install()


async def send_command(command):
    reader, writer = await asyncio.open_unix_connection('/tmp/lcib.sock')

    writer.write(command.encode('utf-8'))
    writer.close()

    await writer.wait_closed()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('command')
    args = parser.parse_args()

    # print(args)

    if args.command == 'start':
        runpy.run_module('lcib')

    else:
        asyncio.run(send_command(args.command))


import asyncio

class CIBConnection(asyncio.Protocol):
    def __init__(self, queue):
        super().__init__()

        self.queue = queue

    def connection_made(self, transport):
        self.transport = transport


    def data_received(self, data):
        message = data.decode()
        self.queue.put_nowait(message)


class CIBDaemon:

    def __init__(self, queue):
        self.queue = queue
        self.loop = asyncio.get_event_loop()

    def connect(self, *args, **kwargs):
        conn = CIBConnection(self.queue)
        return conn
        # await self.queue.put(*args)

    async def run(self):
        print('starting daemon')
        return await self.loop.create_unix_server(self.connect,
                path='/tmp/lcib.sock')
